import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { Provider } from 'react-redux';


import Navigation from './navigation/AppNavigation';
import { store } from './store';

console.ignoredYellowBox = [ 'Setting a timer' ];

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Navigation />
			</Provider>
		);
	};
};

export default App;