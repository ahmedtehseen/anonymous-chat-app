import firebase from 'firebase';

const config = {
	apiKey: "AIzaSyDWQhrOeTG9abNLzdUYWs2pKhZx4-10Fx4",
	authDomain: "pintextapp.firebaseapp.com",
	databaseURL: "https://pintextapp.firebaseio.com",
	projectId: "pintextapp",
	storageBucket: "pintextapp.appspot.com",
	messagingSenderId: "969263877434"
};

export default firebase.initializeApp(config);
