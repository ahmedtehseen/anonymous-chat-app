import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import { reactReduxFirebase } from 'react-redux-firebase';
import { createLogger } from 'redux-logger';

import reducers from '../reducers';
import firebase from '../firebase';
import { epicMiddleware } from '../epics';

const rrfConfig = {
	userProfile: 'users',
	enableLogging: false,
	ReactNative: { AsyncStorage },
	enableRedirectHandling: false,
	onAuthStateChanged: (authData, firebase, dispatch) => {
		console.log('authData', authData);
		console.log('firebase', firebase);
		console.log('dispatch', dispatch);
	},
};

const logger = createLogger({
	collapsed: true,
});

export const store = createStore(reducers,
	{},
	compose(
		reactReduxFirebase(firebase, rrfConfig),
		applyMiddleware(logger, epicMiddleware),
		global.reduxNativeDevTools ?
			global.reduxNativeDevTools(/* options*/) :
			noop => noop
	));

if (global.reduxNativeDevTools) {
	global.reduxNativeDevTools.updateStore(store);
}
