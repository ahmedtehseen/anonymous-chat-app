import {CHANGE_ACTION} from '../actions'

const INITIAL_STATE = {}

export default (state = INITIAL_STATE, action) => {
	switch(action.type) {
		case CHANGE_ACTION:
			return { ...state, text:action.payload};
		default:
			return state;
	}
} 