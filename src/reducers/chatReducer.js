import { SELECTED_CHAT, SELECTED_PIN_STATUS, SELECTED_PIN_BLOCK, GET_CHAT, GET_CHAT_SUCCESS, REFRESH_CHAT } from '../actions';
import { GiftedChat } from 'react-native-gifted-chat';

const INITIAL_STATE = {
	selectedPin: null,
	selectedPinStatus: null,
	selectedPinBlock: null,
	activeChat: [],
	isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
	case SELECTED_CHAT:
		return { ...state, selectedPin: action.payload };
	case SELECTED_PIN_STATUS:
		return { ...state, selectedPinStatus: action.payload };
	case SELECTED_PIN_BLOCK:
		return { ...state, selectedPinBlock: action.payload };
	case GET_CHAT:
		return Object.assign({}, state, { isLoading: true });
	case GET_CHAT_SUCCESS:
		return Object.assign({}, state, {
			activeChat: GiftedChat.append(state.activeChat, action.payload),
			isLoading: false,
		});
	case REFRESH_CHAT:
		return INITIAL_STATE;
	default:
		 return state;
	}
};
