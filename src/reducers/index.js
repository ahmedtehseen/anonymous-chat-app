import { combineReducers } from 'redux';
import { firebaseStateReducer as firebase } from 'react-redux-firebase';
import HomeReducer from './homeReducer';
import LoginReducer from './loginReducer';
import SignupReducer from './signupReducer';
import PinReducer from './pinReducer';
import ChatReducer from './chatReducer';

export default combineReducers({
	home: HomeReducer,
	auth: LoginReducer,
	signup: SignupReducer,
	pins: PinReducer,
	chat: ChatReducer,
	firebase,
});
