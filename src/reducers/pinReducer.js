import {
	GET_PINS,
	GET_PINS_SUCCESS,
	GET_PINS_FAIL,
	GET_PIN_SUCCESS,
	ADD_PIN,
	ADD_PIN_SUCCESS,
	ADD_PIN_FAIL,
} from '../actions';

const INITIAL_STATE = {
	userPins: [],
	isLoading: false,
	error: null,
	newPin: null,
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
	case GET_PIN_SUCCESS:
		return { ...state, newPin: action.payload };
	case GET_PINS:
		return { ...state, isLoading: true };
	case GET_PINS_SUCCESS:
		return { ...state, userPins: action.payload, isLoading: false };
	case ADD_PIN:
		return { ...state, isLoading: true };
	case ADD_PIN_SUCCESS:
		return { ...state, isLoading: false };
	case GET_PINS_FAIL:
	case ADD_PIN_FAIL:
		return { ...state, error: action.payload, isLoading: false };
	default:
		return state;
	}
};
