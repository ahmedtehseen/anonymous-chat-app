import { SET_USER_PROFILE } from '../actions';

const INITIAL_STATE = {
	isLoggedIn: false
}

export default (state = INITIAL_STATE, action) => {
	switch(action.type) {
		case SET_USER_PROFILE:
			return {...state, isLoggedIn: true , user:action.payload}
		default:
			return state;
	}
}