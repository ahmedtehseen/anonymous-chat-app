import { CREATE_USER, CREATE_USER_SUCCESS, CREATE_USER_FAIL } from '../actions';

const INITIAL_STATE = {
	isLoading: false,
	isRegistered: false,
	error: null,
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
	case CREATE_USER:
		return { ...state, isLoading: true };
	case CREATE_USER_SUCCESS:
		return { ...state, isLoading: false, isRegistered: true };
	case CREATE_USER_FAIL:
		return { ...state, isLoading: false, error: action.payload };
	default:
		return state;
	}
};
