import React from 'react';
import { ScrollView } from 'react-native';
import { StackNavigator, DrawerNavigator, DrawerItems } from 'react-navigation';
import { Home } from '../screens/homeScreen';
import { Login } from '../screens/loginScreen';
import { Signup } from '../screens/signupScreen';
import { PinList } from '../screens/pinScreen';
import { Chat } from '../screens/chatScreen';
import { Reset } from '../screens/resetScreen';
import { Profile } from '../screens/profileScreen';

const drawerNavigation = DrawerNavigator({
	UserScreen: { screen: PinList },
	ProfileScreen: { screen: Profile },
}, {
	contentComponent: props => <ScrollView><DrawerItems {...props} /></ScrollView>,
	contentOptions: {
		activeTintColor: '#FF69B4',
		style: {
			flex: 1,
			paddingTop: 15,
		},
	},
});

const Navigation = StackNavigator({
	HomeScreen: { screen: Home },
	UserScreen: { screen: drawerNavigation },
	SignupScreen: { screen: Signup },
	LoginScreen: { screen: Login },
	ResetScreen: { screen: Reset },
	ChatScreen: { screen: Chat },
}, {
	headerMode: 'screen',
	initialRouteName: 'HomeScreen',
});

export default Navigation;
