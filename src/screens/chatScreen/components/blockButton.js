import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { getFirebase } from 'react-redux-firebase';

// store
import { store } from '../../../store';

class BlockButtonComponent extends Component {
	render() {
		if (!this.props.selectedPinBlock) {
			return (
				<View>
					<Text style={{ color: 'red', padding: 10, backgroundColor: 'white', marginRight: 10 }}>
            Blocked
					</Text>
				</View>
			);
		}
		return (
			<View>
				<TouchableOpacity onPress={() => BlockUnblockUser()}>
					<Text style={{ color: '#69B4FF', padding: 10, backgroundColor: 'white', marginRight: 10 }}>
						{this.props.selectedPinStatus ? 'Block' : 'Unblock' }
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

function BlockUnblockUser() {
	const blockUserPin = store.getState().chat.selectedPin.replace(/\./g, '-');
	const myPin = store.getState().auth.user.pin.replace(/\./g, '-');
	console.log('store.getState().chat.selectedPinStatus', store.getState().chat.selectedPinStatus);
	getFirebase().ref(`pins/${myPin}/${blockUserPin}`).set(!store.getState().chat.selectedPinStatus);
}

const mapStateToProps = (state) => ({
	selectedPinStatus: state.chat.selectedPinStatus,
	selectedPinBlock: state.chat.selectedPinBlock,
});

export default connect(mapStateToProps, { })(BlockButtonComponent);
