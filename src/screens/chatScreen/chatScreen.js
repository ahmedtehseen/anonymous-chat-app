import React, { Component } from 'react';
import { Text, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import { GiftedChat, Send } from 'react-native-gifted-chat';

// actions
import { sendMessage, getChat, refreshChat } from '../../actions';

// components
import BlockButton from './components/blockButton';

// styles
// import styles from './homeScreenStyles';

class ChatScreen extends Component {
		static navigationOptions = ({ navigation }) => ({
			title: `${navigation.state.params.title}`,
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
			},
			headerRight: <BlockButton />,
		})

		state = {
			messages: [],
			text: '',
		};

		componentWillMount() {
			const myPin = this.props.user.pin;
			const { selectedPin } = this.props;
			const alteredMyPin = myPin.replace(/\./g, '-');
			const alteredSelectedPin = selectedPin.replace(/\./g, '-');
			this.props.getChat(alteredMyPin, alteredSelectedPin);
		}

		componentWillUnmount() {
			this.props.refreshChat();
		}

		onSend(messages) {
			if (!this.props.selectedPinBlock) return;
			const { text } = messages[0];
			const myPin = this.props.user.pin;
			const { selectedPin } = this.props;
			const alteredMyPin = myPin.replace(/\./g, '-');
			const alteredSelectedPin = selectedPin.replace(/\./g, '-');
			const timestamp = this.props.firebase.database.ServerValue.TIMESTAMP;
			this.props.sendMessage(alteredMyPin, alteredSelectedPin, text, timestamp);
		}

		setCustomText(text) {
			this.setState({
				text,
			});
		}

		handleGotPinText(navigate) {
			navigate('LoginScreen');
		}

		handleGetPinText(navigate) {
			navigate('SignupScreen');
		}

		render() {
			const { navigate } = this.props.navigation;
			const SendButton = (props) => (
				<Send {...props} style={{ display: this.props.selectedPinBlock ? 'block' : 'none' }}>
					<Text>
            Send
					</Text>
				</Send>
			);
			if (this.props.isLoading) {
				return (
					<ScrollView contentContainerStyle={{
						display: 'flex',
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center',
						backgroundColor: '#fff',
					}}
					>
						<ActivityIndicator color="#FF69B4" size="large" />
					</ScrollView>
				);
			}
			return (
				<GiftedChat
  					renderSend={SendButton} messages={this.props.activeChat} text={this.state.text} onInputTextChanged={(text) => this.setCustomText(text)} onSend={(messages) => this.onSend(messages)} user={{ _id: 1 }}
				/>
			);
		}
}

const wrappedChatScreen = firebaseConnect()(ChatScreen);

const mapStateToProps = (state) => ({
	selectedPin: state.chat.selectedPin,
	selectedPinBlock: state.chat.selectedPinBlock,
	user: state.auth.user,
	activeChat: state.chat.activeChat,
	isLoading: state.chat.isLoading,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	sendMessage,
	getChat,
	refreshChat,
}, dispatch);

export const Chat = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedChatScreen);
