import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
	column: {
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
		width: '100%',
	},
	header: {
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 25,
		paddingBottom: 17,
		width: '100%',
	},
	bordered: {
		borderTopWidth: 1,
		borderColor: '#FF69B4',
	},
	avatar: {
		alignSelf: 'center',
		height: 150,
		width: 150,
		borderWidth: 5,
		borderRadius: 75,
		borderColor: '#69B4FF',
	},
	text: {
		padding: 10,
		margin: 5,
		backgroundColor: '#ecf0f1',
		borderRadius: 25,
		color: '#69B4FF',
	},
	buttonContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		marginBottom: 20,
		marginTop: 20,
	},
	button: {
		backgroundColor: '#69B4FF',
		padding: 8,
		textAlign: 'center',
		color: 'white',
		borderRadius: 2,
		marginRight: 10,
	},
});
