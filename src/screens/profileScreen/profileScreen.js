import React from 'react';
import { ScrollView, View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import Icon from 'react-native-vector-icons/Entypo';

import styles from './profileScreenStyle';

export class ProfileScreen extends React.Component {
		static navigationOptions = ({ navigation }) => ({
			title: 'Profile',
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
			},
			headerLeft: (
				<TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')}>
					<Icon name="menu" size={30} color="white" style={{ marginLeft: 10 }} />
				</TouchableOpacity>
			),
		});

		resetNavigation(targetRoute) {
			const resetAction = NavigationActions.reset({
				index: 0,
				actions: [
					NavigationActions.navigate({ routeName: targetRoute }),
				],
			});
			this.props.navigation.dispatch(resetAction);
		}

		handleLogout() {
			this.props.firebase.logout()
				.then(() => this.resetNavigation('LoginScreen'))
				.catch((err) => console.log(err));
		}

		render() {
			const { user } = this.props;
			const TextBox = (prop) => (
				prop.text ? <Text style={styles.text}>{prop.text}</Text> : null
			);
			if (!user) {
				return (
					<ScrollView contentContainerStyle={styles.column}>
						<ActivityIndicator color="#FF69B4" size="large" />
					</ScrollView>
				);
			}
			return (
				<ScrollView contentContainerStyle={styles.column}>
					<View style={styles.header}>
						<Image source={{ uri: 'https://www.artconversation.com/core/images/default/default_avatar_large.png' }} style={styles.avatar} />
					</View>
					<View style={[styles.header, styles.bordered]}>
						<TextBox text={user.name} />
						<TextBox text={user.email} />
						<TextBox text={user.pin} />
						<View style={styles.buttonContainer}>
							<TouchableOpacity onPress={() => this.handleLogout()}>
								<Text style={styles.button}>
                Logout
			        </Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			);
		}
}

const wrappedProfileScreen = firebaseConnect()(ProfileScreen);

const mapStateToProps = (state) => ({
	user: state.auth.user,
	firebase: state.firebase,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export const Profile = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedProfileScreen);
