import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect, getFirebase } from 'react-redux-firebase';
import t from 'tcomb-form-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// actions
import { getPin } from '../../actions';

// styles
import styles from './signupScreenStyles';

class SignupScreen extends Component {
		static navigationOptions = {
			title: 'Signup',
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
				display: 'flex',
			},
			headerTitleStyle: {
				alignSelf: 'center',
				marginLeft: 20,
			},
			headerRight: <View />,
		}

		componentWillReceiveProps(nextProps) {
			if (nextProps.isRegistered) {
				this.resetNavigation('LoginScreen');
			}
		}

		resetNavigation(targetRoute) {
			const resetAction = NavigationActions.reset({
				index: 0,
				actions: [
					NavigationActions.navigate({ routeName: targetRoute }),
				],
			});
			this.props.navigation.dispatch(resetAction);
		}

		handleSignup() {
			const user = this.refs.signupForm.getValue();
			if (user) {
				const { email, password } = user;
				const timestamp = this.props.firebase.database.ServerValue.TIMESTAMP;
				this.props.getPin({ email, password, timestamp });
			}
		}

		render() {
			if (this.props.isLoading) {
				return (
					<ScrollView contentContainerStyle={styles.column}>
						<ActivityIndicator color="#FF69B4" size="large" />
					</ScrollView>
				);
			}
			return (
				<KeyboardAwareScrollView contentContainerStyle={styles.column}>
					<Text
						style={{
							fontSize: 30,
							marginBottom: 10,
						}}
					>
          Sign Up
					</Text>
					<View style={{ width: '80%' }}>
						<Form
							ref="signupForm"
							type={SignupForm}
							options={options}
						/>
					</View>
					<View style={styles.buttonContainer}>
						<TouchableOpacity onPress={() => this.handleSignup()}>
							<Text style={styles.button}>
              Sign Up
							</Text>
						</TouchableOpacity>
					</View>
				</KeyboardAwareScrollView>
			);
		}
}

const wrappedSignupScreen = firebaseConnect()(SignupScreen);

const Form = t.form.Form;

const SignupForm = t.struct({
	email: t.String,
	password: t.String,
});

const options = {
	fields: {
		email: {
			keyboardType: 'email-address',
		},
		password: {
			secureTextEntry: true,
		},
	},
};

const mapStateToProps = (state) => ({
	isLoading: state.signup.isLoading,
	isRegistered: state.signup.isRegistered,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	getPin,
}, dispatch);

export const Signup = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedSignupScreen);
