import {StyleSheet, Dimensions} from 'react-native';


export default StyleSheet.create({
	column: {
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
    backgroundColor: '#fff',
	},
	buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: 20,
    marginTop: 20
	},
	button: {
    backgroundColor: '#69B4FF',
    padding: 8,
    fontWeight: '500',
    textAlign:'center',
    color: 'white',
    borderRadius: 2,
    fontSize: 18,
    width: 200,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    marginBottom: 10,
    elevation: 2,
	}
})