import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import t from 'tcomb-form-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// actions
import { resetSuccess, resetFail } from '../../actions';

// styles
import styles from './resetScreenStyle';

class ResetScreen extends Component {
		static navigationOptions = {
			title: 'Reset',
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
				display: 'flex',
			},
			headerTitleStyle: {
				alignSelf: 'center',
			},
			headerRight: <View />,
		}

		state = {
			value: {
				email: 'abc@abc.com',
			},
			isLoading: false,
		}

		resetNavigation(targetRoute) {
			const resetAction = NavigationActions.reset({
				index: 0,
				actions: [
					NavigationActions.navigate({ routeName: targetRoute }),
				],
			});
			this.props.navigation.dispatch(resetAction);
		}

		handleReset() {
			const value = this.refs.resetForm.getValue();
			if (value) {
				this.setState({ isLoading: true });
				console.log('reset', value);
				this.props.firebase.resetPassword(value.email)
					.then(res => {
						console.log('reset res', res);
						this.props.navigation.navigate('LoginScreen');
					})
					.catch(err => this.props.resetFail(err));
			}
		}

		render() {
			if (this.state.isLoading) {
				return (
					<ScrollView contentContainerStyle={styles.column}>
						<ActivityIndicator color="#FF69B4" size="large" />
					</ScrollView>
				);
			}
			return (
				<KeyboardAwareScrollView contentContainerStyle={styles.column}>
					<Text style={{ fontSize: 30, marginBottom: 10 }} >
          Reset Password
					</Text>
					<View style={{ width: '80%' }}>
						<Form ref="resetForm" type={ResetForm} options={options} value={this.state.value} />
					</View>
					<View style={styles.buttonContainer}>
						<TouchableOpacity onPress={() => this.handleReset()}>
							<Text style={styles.button}>
              Reset Password
							</Text>
						</TouchableOpacity>
					</View>
				</KeyboardAwareScrollView>
			);
		}
}

const wrappedReset = firebaseConnect()(ResetScreen);

const Form = t.form.Form;
const ResetForm = t.struct({
	email: t.String,
});
const options = {
	fields: {
		email: {
			keyboardType: 'email-address',
		},
	},
};

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	resetSuccess,
	resetFail,
}, dispatch);

export const Reset = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedReset);
