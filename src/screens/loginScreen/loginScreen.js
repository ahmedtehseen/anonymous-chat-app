import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import t from 'tcomb-form-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// actions
import { loginSuccess, loginFail } from '../../actions';
// styles
import styles from './loginScreenStyles';

class LoginScreen extends Component {
	static navigationOptions = {
		title: 'Login',
		headerTintColor: '#fff',
		headerStyle: {
			backgroundColor: '#FF69B4',
			display: 'flex',
		},
		headerTitleStyle: {
			alignSelf: 'center',
		},
		headerRight: <View />,
	}

	state = {
		value: {
			email: 'abc@abc.com',
			password: '123456',
		},
		isLoading: false,
	}

	resetNavigation(targetRoute) {
		const resetAction = NavigationActions.reset({
			index: 0,
			actions: [
				NavigationActions.navigate({ routeName: targetRoute }),
			],
		});
		this.props.navigation.dispatch(resetAction);
	}

	handleLogin() {
		const value = this.refs.loginForm.getValue();
		if (value) {
			this.setState({ isLoading: true });
			this.props.firebase.login(value)
				.then(res => {
					this.props.loginSuccess(res.uid);
					this.resetNavigation('UserScreen');
				})
				.catch(err => this.props.loginFail(err));
		}
	}

	handleReset() {
		this.props.navigation.navigate('ResetScreen');
	}

	render() {
		if (this.state.isLoading) {
			return (
				<ScrollView contentContainerStyle={styles.column}>
					<ActivityIndicator color="#FF69B4" size="large" />
				</ScrollView>
			);
		}
		return (
			<KeyboardAwareScrollView contentContainerStyle={styles.column}>
				<Text style={{ fontSize: 30, marginBottom: 10 }} >
					Sign In
				</Text>
				<View style={{ width: '80%' }}>
					<Form ref="loginForm" type={LoginForm} options={options} value={this.state.value} />
				</View>
				<View style={styles.buttonContainer}>
					<TouchableOpacity onPress={() => this.handleLogin()}>
						<Text style={styles.button}>
							Sign In
						</Text>
					</TouchableOpacity>
				</View>
				<TouchableOpacity onPress={() => this.handleReset()}>
					<Text style={styles.buttonSecondary}>
						Forget Passowrd?
					</Text>
				</TouchableOpacity>
			</KeyboardAwareScrollView>
		);
	}
}

const wrappedLoginScreen = firebaseConnect()(LoginScreen);

const Form = t.form.Form;
const LoginForm = t.struct({
	email: t.String,
	password: t.String,
});
const options = {
	fields: {
		email: {
			keyboardType: 'email-address',
		},
		password: {
			secureTextEntry: true,
		},
	},
};

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loginSuccess,
	loginFail,
}, dispatch);

export const Login = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedLoginScreen);
