import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';
import { selectedChat } from '../../../actions';

class ListItem extends Component {
	onRowPress(userPin) {
		this.props.selectedChat(userPin);
		this.props.navigate('ChatScreen', { title: userPin });
	}

	render() {
		const { userPin, myPin } = this.props;
		return (
			<TouchableOpacity onPress={() => this.onRowPress(userPin)}>
				<View style={styles.listItem}>
					<Icon name="message" size={30} />
					<Text style={styles.titleStyle}>
						{userPin}
					</Text>
					<Icon name="chevron-right" size={30} />
				</View>
			</TouchableOpacity>
		);
	}
}

const styles = {
	titleStyle: {
		fontSize: 18,
		fontWeight: '500',
	},
	listItem: {
		flexDirection: 'row',
		// backgroundColor: '',
		justifyContent: 'space-around',
		alignItems: 'center',
		height: 50,
		marginTop: 10,
	},
};

export default connect(null, { selectedChat })(ListItem);
