import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import Modal from 'react-native-modal';
import ActionButton from 'react-native-action-button';
import t from 'tcomb-form-native';
import Icon from 'react-native-vector-icons/Entypo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// custom components
import ListItem from './components/listItems';
import Separator from './components/listSeparator';

// actions
import { addPin, getUsersPin } from '../../actions';

// styles
import styles from './pinScreenStyles';

class PinScreen extends Component {
		static navigationOptions = ({ navigation }) => ({
			title: 'Pins',
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
			},
			headerLeft: (
				<TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')}>
					<Icon name="menu" size={30} color="white" style={{ marginLeft: 10 }} />
				</TouchableOpacity>
			),
		})

		state = {
			isModalVisible: false,
			pinValue: '',
		};

		_showModal = () => this.setState({ isModalVisible: true })

		_hideModal = () => this.setState({ isModalVisible: false })

		addPin() {
			const struct = this.refs.pin.getValue();
			const myPin = this.props.user.pin;
			if (!struct) return;
			this.props.addPin(myPin, struct.pin);
			this.setState({ pinValue: '' });
			this._hideModal();
		}
		render() {
			const { navigate } = this.props.navigation;
			if (this.props.isLoading) {
				return (
					<ScrollView contentContainerStyle={styles.column}>
						<ActivityIndicator color="#FF69B4" size="large" />
					</ScrollView>
				);
			}
			return (
				<KeyboardAwareScrollView contentContainerStyle={styles.column}>
					<ScrollView contentContainerStyle={{ width: 350 }}>
						<FlatList
  data={this.props.pinList}
  renderItem={({ item }) => (
								<ListItem userPin={item.pin} myPin={this.props.user.pin} navigate={navigate} />
							)}
  keyExtractor={item => item.pin}
  ItemSeparatorComponent={Separator}
						/>
					</ScrollView>
					<ActionButton buttonColor="#FF69B4" onPress={this._showModal} />
					<Modal
						isVisible={this.state.isModalVisible}
						onBackButtonPress={this._hideModal}
						onBackdropPress={this._hideModal}
						animationInTiming={500}
						animationOutTiming={500}
						value={this.state.pinValue}
						onChange={(pinValue) => this.setState({ pinValue })}
					>
						<Text style={styles.modalHeader}>New Conversation</Text>
						<View style={styles.modal}>
							<Form
  ref="pin"
  type={AddPin}
  options={{
									fields: {
										pin: {
											keyboardType: 'numeric',
											error: 'Please enter valid pin number.',
										},
									},
								}}
							/>
							<TouchableOpacity onPress={() => this.addPin()}>
								<Text style={[styles.button, { width: '100%' }]}>
                Start
			        </Text>
							</TouchableOpacity>
						</View>
					</Modal>
				</KeyboardAwareScrollView>
			);
		}
}

const Phone = t.subtype(t.String, (phone) => {
	const reg = /^\d{3}.\d{3}.\d{3}$/;
	return reg.test(phone);
});

const Form = t.form.Form;
const AddPin = t.struct({
	pin: Phone,
});

const wrappedPinScreen = firebaseConnect()(PinScreen);

const mapStateToProps = (state) => ({
	user: state.auth.user,
	pinList: state.pins.userPins,
	isLoading: state.pins.isLoading,
	error: state.pins.error,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	addPin,
	getUsersPin,
}, dispatch);

export const PinList = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedPinScreen);
