import {StyleSheet, Dimensions} from 'react-native';


export default StyleSheet.create({
	column: {
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
    backgroundColor: '#fff',
	},
	buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: 20,
    marginTop: 20
	},
	button: {
    backgroundColor: '#69B4FF',
    padding: 8,
    textAlign:'center',
    color: 'white',
    borderRadius: 2,
    marginRight: 10,
	},
  modal: {
    backgroundColor: '#fff',
    padding: 10,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  modalHeader: {
    backgroundColor: '#FF69B4',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
    margin: 0,
    color: '#fff',
    padding: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  }
})