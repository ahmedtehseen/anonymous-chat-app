import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
// actions
import { } from '../../actions';
// styles
import styles from './homeScreenStyles';

class HomeScreen extends Component {
		static navigationOptions = {
			title: 'Safe, Simple Communication',
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#FF69B4',
				display: 'flex',
			},
			headerTitleStyle: { alignSelf: 'center' },
		}

		handleSignup(navigate) {
			navigate('SignupScreen');
		}

		handleSignin(navigate) {
			navigate('LoginScreen');
		}

		handleHome(navigate) {
			navigate('UserScreen');
		}

		render() {
			const { navigate } = this.props.navigation;
			return (
				<View style={styles.column}>
					<Image source={require('../../assets/images/logo.png')} style={{ marginBottom: 10 }} />
					<Text style={{ fontSize: 30, marginBottom: 10 }} >
          PINtext
					</Text>
					<Text style={{ textAlign: 'center', marginBottom: 30 }} >
          The safe, simple way to exchange information while protecting your identity.
					</Text>
					<View style={styles.buttonContainer}>
						<TouchableOpacity onPress={() => this.handleSignup(navigate)}>
							<Text style={styles.button}>
              Sigup
							</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.buttonContainer}>
						{
							this.props.auth.isLoaded
								? this.props.auth.isEmpty
									? <TouchableOpacity onPress={() => this.handleSignin(navigate)}>
										<Text style={[styles.button, { backgroundColor: '#FF69B4' }]}>
                    Sigin
										</Text>
									</TouchableOpacity>
									: <TouchableOpacity onPress={() => this.handleHome(navigate)}>
										<Text style={[styles.button, { backgroundColor: '#FF69B4' }]}>
                    Home
										</Text>
									</TouchableOpacity>
								: <ActivityIndicator color="#FF69B4" size="large" />
						}
					</View>
				</View >
			);
		}
}

const wrappedHomeScreen = firebaseConnect()(HomeScreen);

const mapStateToProps = (state) => ({
	auth: state.firebase.auth,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export const Home = connect(
	mapStateToProps,
	mapDispatchToProps
)(wrappedHomeScreen);
