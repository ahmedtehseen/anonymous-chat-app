import { Observable } from 'rxjs';
import { getFirebase } from 'react-redux-firebase';
import {
	USER_LOGIN,
	USER_LOGIN_SUCCESS,
	USER_LOGIN_FAIL,
	SET_USER_PROFILE,
	GET_PINS,
} from '../actions';

export class LoginEpic {
	static setUserProfile = (action$) =>
		action$.ofType('@@reactReduxFirebase/SET_PROFILE')
			.switchMap(({ profile }) => {
				if (profile !== null) {
					const { email, pin } = profile;
					const isLoggedIn = true;
					const profileObject = { email, pin, isLoggedIn };
					return Observable.concat(
						Observable.of({
							type: SET_USER_PROFILE,
							payload: profileObject,
						}),
						Observable.of({
							type: GET_PINS,
							payload: profileObject.pin.replace(/\./g, '-'),
						})
					);
				}
				return Observable.of({
					type: 'USER_CREATED_SUCCESSFULLY',
					message: 'Please Login to continue..!',
				});
			}).catch(err => {
				console.log('user profile error', err);
				return Observable.of({
					type: USER_LOGIN_FAIL,
					payload: err,
				});
			})
}
