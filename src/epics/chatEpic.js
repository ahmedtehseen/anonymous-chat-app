import { Observable } from 'rxjs';
import { getFirebase } from 'react-redux-firebase';
import {
	SEND_MESSAGE,
	SEND_MESSAGE_SUCCESS,
	SEND_MESSAGE_FAIL,
	SELECTED_CHAT,
	SELECTED_PIN_STATUS,
	SELECTED_PIN_BLOCK,
	GET_CHAT,
	GET_CHAT_SUCCESS,
	GET_CHAT_FAIL,
} from '../actions';

export class ChatEpic {
	static sendMessage = (action$) =>
		action$.ofType(SEND_MESSAGE)
			.switchMap(({ payload }) => new Observable(observer => {
				getFirebase().ref(`chats/${payload.selectedPin},${payload.myPin}`).once('value', snap => {
					if (snap.exists()) {
						getFirebase().push(`chats/${payload.selectedPin},${payload.myPin}`, {
							from: payload.myPin,
							message: payload.text,
							timestamp: payload.timestamp,
							// ".priority": 1-Date.now()
						});
					} else {
						getFirebase().push(`chats/${payload.myPin},${payload.selectedPin}`, {
							from: payload.myPin,
							message: payload.text,
							timestamp: payload.timestamp,
							// ".priority": 1-Date.now()
						});
					}
				});
			}))

		static getSelectedPinStatus = (action$, store) =>
			action$.ofType(SELECTED_CHAT)
				.switchMap(({ payload }) => new Observable(observer => {
					const blockUserPin = payload.replace(/\./g, '-');
					const myPin = store.getState().auth.user.pin.replace(/\./g, '-');
					getFirebase().ref(`pins/${myPin}/${blockUserPin}`).on('value', snap => {
						observer.next({ type: SELECTED_PIN_STATUS, payload: snap.val() });
					});
				}))

		static getSelectedPinBlock = (action$, store) =>
			action$.ofType(SELECTED_CHAT)
				.switchMap(({ payload }) => new Observable(observer => {
					const blockUserPin = payload.replace(/\./g, '-');
					const myPin = store.getState().auth.user.pin.replace(/\./g, '-');
					getFirebase().ref(`pins/${blockUserPin}/${myPin}`).on('value', snap => {
						observer.next({ type: SELECTED_PIN_BLOCK, payload: snap.val() });
					});
				}))

	  static getChat = (action$) =>
				action$.ofType(GET_CHAT)
					.switchMap(({ payload }) => new Observable(observer => {
						getFirebase().ref(`chats/${payload.selectedPin},${payload.myPin}`).once('value', snap => {
							if (snap.exists()) {
								getFirebase().ref(`chats/${payload.selectedPin},${payload.myPin}`).on('child_added', snapshot => {
									const snap = snapshot.val();
									const snapKey = snapshot.key;
									const messageObj = Object.assign({}, {
										_id: snapKey,
										text: snap.message,
										createdAt: new Date(snap.timestamp),
										user: {
											_id: snap.from === payload.myPin ? 1 : 2,
											name: snap.from === payload.myPin ? '' : 'Pin Sender',
										},
									});
									// console.log('cheking obj',messageObj)
									observer.next({ type: GET_CHAT_SUCCESS, payload: messageObj });
								});
							} else {
								getFirebase().ref(`chats/${payload.myPin},${payload.selectedPin}`).once('value', snap => {
									if (!snap.exists()) observer.next({ type: GET_CHAT_SUCCESS, payload: [] });
									getFirebase().ref(`chats/${payload.myPin},${payload.selectedPin}`).on('child_added', snapshot => {
										const snap = snapshot.val();
										const snapKey = snapshot.key;
										const messageObj = Object.assign({}, {
											_id: snapKey,
											text: snap.message,
											createdAt: new Date(snap.timestamp),
											user: {
												_id: snap.from === payload.myPin ? 1 : 2,
												name: snap.from === payload.myPin ? '' : 'Pin Sender',
											},
										});
										observer.next({ type: GET_CHAT_SUCCESS, payload: messageObj });
									});
								});
							}
						});
					}))
}
