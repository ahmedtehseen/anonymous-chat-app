import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { LoginEpic } from './loginEpic';
import { SignupEpic } from './signupEpic';
import { PinEpic } from './pinEpic';
import { ChatEpic } from './chatEpic';

const epics = combineEpics(
	LoginEpic.setUserProfile,
	SignupEpic.createUser,
	PinEpic.addPinEpic,
	PinEpic.getPinEpic,
	PinEpic.getUsersPin,
	ChatEpic.sendMessage,
	ChatEpic.getSelectedPinStatus,
	ChatEpic.getSelectedPinBlock,
	ChatEpic.getChat
);

export const epicMiddleware = createEpicMiddleware(epics);
