import { Observable } from 'rxjs';
import { getFirebase } from 'react-redux-firebase';

import {
	ADD_PIN,
	ADD_PIN_SUCCESS,
	ADD_PIN_FAIL,
	CREATE_USER,
	GET_PIN,
	GET_PIN_SUCCESS,
	GET_PIN_FAIL,
	GET_PINS,
	GET_PINS_SUCCESS,
	GET_PINS_FAIL,
} from '../actions';

export class PinEpic {
		static addPinEpic = (action$) =>
			action$.ofType(ADD_PIN)
				.switchMap((action) => new Observable(observer => {
					getFirebase().ref('users').orderByChild('pin').equalTo(action.pinToAdd).once('value', (snap) => {
						if (snap.val()) {
							console.log('snap', snap.val());
							const myPin = action.myPin.replace(/\./g, '-');
							const pinToAdd = action.pinToAdd.replace(/\./g, '-');
							const updatedData = {};
							updatedData[`pins/${myPin}/${pinToAdd}`] = true;
							updatedData[`pins/${pinToAdd}/${myPin}`] = true;
							getFirebase().ref().update(updatedData)
								.then(res => {
									observer.next({
										type: ADD_PIN_SUCCESS,
										payload: res,
									});
								})
								.catch(err => {
									observer.next({
										type: ADD_PIN_FAIL,
										payload: err,
									});
								});
							return;
						}
						observer.next({
							type: ADD_PIN_FAIL,
							payload: 'No account is associated with this Pin.',
						});
					});
				})).catch(err => {
					console.log('errer', err);
					return Observable.of({
						type: ADD_PIN_FAIL,
						payload: err,
					});
				})

		static getPinEpic = (action$) =>
			action$.ofType(GET_PIN)
				.switchMap((action) => new Observable(observer => {
					const firstValue = Math.floor(100 + Math.random() * 900);
					const secondValue = Math.floor(100 + Math.random() * 900);
					const thirdValue = Math.floor(100 + Math.random() * 900);
					const pin = `${firstValue}.${secondValue}.${thirdValue}`;
					getFirebase().ref('users').orderByChild('pin').equalTo(pin).once('value', (snap) => {
						if (snap.val()) {
							console.log('snap', snap.val());
							return observer.next({
								type: GET_PIN,
							});
						}
						observer.next({
							type: GET_PIN_SUCCESS,
							payload: pin,
						});
						observer.next({
							type: CREATE_USER,
							payload: {
								email: action.payload.email,
								password: action.payload.password,
								timestamp: action.payload.timestamp,
								pin,
							},
						});
					});
				})).catch(err => {
					console.log('errer', err);
					return Observable.of({
						type: GET_PIN_FAIL,
						payload: err,
					});
				})

		static getUsersPin = (action$) =>
			action$.ofType(GET_PINS)
				.switchMap(({ payload }) => new Observable(observer => {
					getFirebase().ref(`pins/${payload}`).on('value', snap => {
						if (snap.exists()) {
							const pinsArray = Object.keys(snap.val());
							const filteredPins = JSON.parse(JSON.stringify(pinsArray).replace(/\-/g, '.'));
							const pinList = filteredPins.map((pin) => Object.assign({}, { pin }));
							observer.next({ type: GET_PINS_SUCCESS, payload: pinList });
						} else {
							observer.next({ type: GET_PINS_SUCCESS, payload: [] });
						}
					});
				})).catch(err => Observable.of({
					type: GET_PINS_FAIL,
					payload: err,
				}))
}
