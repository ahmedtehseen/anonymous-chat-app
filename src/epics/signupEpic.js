import { Observable } from 'rxjs';
import { getFirebase } from 'react-redux-firebase';

import { CREATE_USER, CREATE_USER_SUCCESS, CREATE_USER_FAIL } from '../actions';

export class SignupEpic {
		static createUser = (action$) =>
			action$.ofType(CREATE_USER)
				.switchMap(({ payload: { email, password, timestamp, pin } }) => new Observable(observer => {
					getFirebase().createUser(
						{ email, password },
						{ email, pin, timestamp }
					).then(res => {
						observer.next({ type: CREATE_USER_SUCCESS });
					}).catch(err => {
						console.log('create user error', err);
						observer.next({ type: CREATE_USER_FAIL, payload: err });
					});
				}))
}
