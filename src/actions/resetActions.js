export const RESET = 'RESET';
export const RESET_SUCCESS = 'RESET_SUCCESS';
export const RESET_FAIL = 'RESET_LOGIN_FAIL';

export const resetSuccess = (res) => ({
	type: RESET_SUCCESS,
	payload: res,
	message: 'Reset Successful..!',
});

export const resetFail = (userObject) => ({
	type: RESET_FAIL,
	payload: userObject,
	message: 'Something went wrong..!',
});
