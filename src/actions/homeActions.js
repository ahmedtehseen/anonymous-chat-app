export const CHANGE_ACTION = 'CHANGE_ACTION';

export const HomeAction = () => {
	return {
		type: CHANGE_ACTION,
		payload: 'changed'
	};
};