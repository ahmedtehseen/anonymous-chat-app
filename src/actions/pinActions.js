export const ADD_PIN = 'ADD_PIN';
export const ADD_PIN_SUCCESS = 'ADD_PIN_SUCCESS';
export const ADD_PIN_FAIL = 'ADD_PIN_FAIL';
export const GET_PIN = 'GET_PIN';
export const GET_PIN_SUCCESS = 'GET_PIN_SUCCESS';
export const GET_PIN_FAIL = 'GET_PIN_FAIL';
export const GET_PINS = 'GET_PINS';
export const GET_PINS_SUCCESS = 'GET_PINS_SUCCESS';
export const GET_PINS_FAIL = 'GET_PINS_FAIL';

export const addPin = (myPin, pinToAdd) => ({
	type: ADD_PIN,
	myPin,
	pinToAdd,
});

export const addPinSuccess = (res) => ({
	type: ADD_PIN_SUCCESS,
	payload: res,
});

export const getPin = (newUser) => ({
	type: GET_PIN,
	payload: newUser,
});

export const getUsersPin = (myPin) => ({
	type: GET_PINS,
	payload: myPin,
});
