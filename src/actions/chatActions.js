export const SELECTED_CHAT = 'SELECTED_CHAT';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAIL = 'SEND_MESSAGE_FAIL';
export const SELECTED_PIN_STATUS = 'SETLECTED_PIN_STATUS';
export const SELECTED_PIN_BLOCK = 'SELECTED_PIN_BLOCK';
export const GET_CHAT = 'GET_CHAT';
export const GET_CHAT_SUCCESS = 'GET_CHAT_SUCCESS';
export const GET_CHAT_FAIL = 'GET_CHAT_FAIL';
export const REFRESH_CHAT = 'REFRESH_CHAT';

export const selectedChat = (userPin) => ({
	type: SELECTED_CHAT,
	payload: userPin,
});

export const sendMessage = (myPin, selectedPin, text, timestamp) => ({
	type: SEND_MESSAGE,
	payload: { myPin, selectedPin, text, timestamp },
});

export const getChat = (myPin, selectedPin) => ({
	type: GET_CHAT,
	payload: { myPin, selectedPin },
});

export const refreshChat = () => ({
	type: REFRESH_CHAT,
});
